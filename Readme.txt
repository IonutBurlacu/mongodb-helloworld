These are the files needed to build a mongodb docker image.

Instructions:

### Cd to the directory with mongodb files (mongodb, Dockerfile, etc.)
1. cd /path/to/mongodb/dir

### Remove the old image in case you have one already
2. docker rmi -f mongo-image:0.1.0

### Build a new image
3. docker build -t mongo-image:0.1.0 .

### Remove the old container if you have one already. 
4. docker rm -f mongo-container

### Run a new container. It will start in daemon mode, this means you will not have access to the container unless you execute the 6th command. The `pwd` variable means the current directory in ubuntu, it might differ in other OS.
5. docker run --name mongo-container -d -t -p 27017:27017 -v `pwd`/mongo:/data/db mongo-image:0.1.0

### To connect to mongo container and execute commands like 'mongo' to see all collection you need to type the following command to enter in the container's bash.
6. docker exec -it mongo-container bash

### To exit from mongo container bash type the following command
7. exit
